// testValues.js
const values = require('../values');
const testObject = require('./testObject');

test('values function', () => {
  expect(values(testObject)).toEqual(['Bruce Wayne', 36, 'Gotham']);
});
