// testKeys.js
const keys = require('../keys');
const testObject = require('./testObject');

test('keys function', () => {
  expect(keys(testObject)).toEqual(['name', 'age', 'location']);
});
