// testPairs.js
const pairs = require('../pairs');
const testObject = require('./testObject');

test('pairs function', () => {
  expect(pairs(testObject)).toEqual([['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']]);
});