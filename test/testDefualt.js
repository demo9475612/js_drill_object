const defaults = require('../defaults');

test('defaults function', () => {
  const obj = { name: 'Bruce Wayne', age: 36 };
  const defaultProps = { age: 30, city: 'Gotham' };
  const newObj = defaults(obj, defaultProps);
  expect(newObj).toEqual({ name: 'Bruce Wayne', age: 36, city: 'Gotham' });
});
