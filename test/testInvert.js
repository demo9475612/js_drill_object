const invert = require('../invert');

test('invert function', () => {
  const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
  const invertedObject = invert(testObject);
  expect(invertedObject).toEqual({ 'Bruce Wayne': 'name', '36': 'age', 'Gotham': 'location' });
});