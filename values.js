function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    return Object.values(obj).filter(value => typeof value !== 'function');
}

module.exports = values;
